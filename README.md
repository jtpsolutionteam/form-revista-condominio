[ ] Máscara para o campo de Whatsapp
[ ] Corrigir bug das mil requisições que o sistema faz para o sistema do IBGE
[X] Option com "Todas as cidades"
[X] Option com "Todos os Estados"
[X] File API com input file
[X] Estilizar o campo de arquivo
[X] Poopover de informações ao lado do "Adicionar outra cidade"
    Texto para por no popover "Caso você preste serviço em outras cidades além do endereço da Matriz, clicar neste botão e informar os demais estados e cidades."